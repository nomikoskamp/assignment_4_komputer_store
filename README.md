# Assignment 1: Komputer Store Create a Dynamic Webpage using JS

## Table of Contents

- [Executive summary](#executive-summary)
- [Technologies](#technologies)
- [General Information](#general-information)
- [Wireframe](#wireframe)
- [Visit Page](#visit-page)
- [Maintainers](#maintainers)
- [License](#license)

## Executive summary

A dynamic webpage of a computer store is builded using vanilla JavaScript.

## Technologies

- [ ] Visual Studio Code
- [ ] Live Server
- [ ] Browser Developer Tools
- [ ] JavaScript, HTML, CSS
- [ ] Figma


## General Information

This web store application has three main sections [Bank](#bank-section), [Work](#work-section), [Buy](#buy-section) with several actions in each section.

### Bank Section

In this section a Bank balance and a Loan amount are shown in USD currency. The Bank balance is the amount available for the user to buy a laptop.
The **Get a loan button** allows the user to enter the amount of the wanted loan.

![alt text1](pics/bank.PNG "Title Text")

### Work Section

In this section the pay of the user is shown in USD currency.
The **Work** button is adding $100 at the Pay balance of the user.
The **Bank** button transfers the money from the user's Pay balance to the bank balance. 
The **Repay** button transfers the full value of user's current Pay to the outstanding Loan.

![alt text1](pics/work.PNG "Title Text")

### Buy Section

This section has 2 parts: 
- [ ] Laptop selection area

User could use the select box to show the available computers and their features.

![alt text1](pics/select.PNG "Title Text")

- [ ] Info selection area

In this field computer's image, description, price and name are shown.
The **BUY NOW** button could be used to buy a computer if there is enough available bank Balance.

![alt text1](pics/buy.PNG "Title Text")


## Wireframe

The following **figure** captures the Wireframe was used for the Komputer Store:

![alt text1](pics/webpage.PNG "Title Text")


## Visit Page

[Komputer Store Website](https://nomikoskamp.gitlab.io/assignment_4_komputer_store/) (click to visit)


## Maintainers

[Nomikos Kampourakis ](https://gitlab.com/nomikoskamp)


## License

Nomikos Kampourakis

@ 2023