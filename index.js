// Elements
const payElement = document.getElementById("pay-amount")
const balanceElement = document.getElementById("balance-amount")
const loanElement = document.getElementById("loan-amount")
const loanTextElement = document.getElementById("loan-text")
const featuresElement = document.getElementById("features-details")
const computerTitleElement = document.getElementById("computer-title")
const computerIconElement = document.getElementById("computer-icon")
const computerPriceElement = document.getElementById("computer-price")
const computerDescriptionElement = document.getElementById("computer-description")
const optionElement = document.getElementById('computer-list')

//  Buttons 
const workButton = document.getElementById("work-button")
const bankButton = document.getElementById("bank-button")
const getLoanButton = document.getElementById("loan-button")
const buyButton = document.getElementById("buy-button")
const repayButton = document.getElementById("repay-button")

// Initialization 
let price = 0
let pay = 0
let balance = 0
let loanAmount = 0
let computers =[]
payElement.innerText = getCurrency(pay)
balanceElement.innerText = getCurrency(balance)
hide(repayButton, loanTextElement)

// JSON Data fetching
const URL = 'https://hickory-quilled-actress.glitch.me/computers'
fetch(URL)
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => {
        renderDetails(computers[0]), addComputersToMenu(computers), handleComputerMenuChange(computers)
    })


// Function that shows data regarding the selected pc
const handleComputerMenuChange = (data) => {
    optionElement.addEventListener('change', function () {              // When a different pc is chosen, 
        renderDetails(data[this.value])                                 // changes the shown data
    })
}


// Initializes elements to show when a computer is chosen
const renderDetails = (featuresData) => {
    featuresElement.innerText = featuresData.specs.join('\n')           // Shows pc's features as a list
    computerTitleElement.innerText = featuresData.title                   // Name of a pc
    // Shows the image of a chosen pc
    computerIconElement.innerHTML = '<img  src="https://hickory-quilled-actress.glitch.me/' + featuresData.image + '" alt="' + featuresData.title + '" style="width:300px;height:230px;">'
    computerPriceElement.innerText = getCurrency(featuresData.price)      // Shows the formatted price of a pc
    computerDescriptionElement.innerText = featuresData.description
    price = featuresData.price                                          // Variable that stores the price of a chosen pc
}


// Initializes the select field element with all pc's given from JSON data
const addComputersToMenu = (data) => {
    for (let i = 0; i < data.length; i++) {
        let option = document.createElement("option")                   // Creates an option element
        option.text = data[i].title                                     // with pc's name
        option.value = i
        optionElement.add(option)                                       // adding this to an option element field
    }
}


// Work button action
function workPayment() {
    pay += 100
    payElement.innerText = getCurrency(pay)
}

workButton.addEventListener("click", workPayment)

// Bnk button action
function bank() {
    if (loanAmount === 0) {                                         // If there is no active loan
        balance += pay                                              // balance increases by pay amount
    }
    else if (loanAmount - 0.1 * pay < 0) {                          // If there is a loan and 10% of pain is grater than loan amount
        balance += 0.9 * pay + Math.abs(loanAmount - 0.1 * pay)     // balance increases by 90% of pay and the rest of loan's 10%, after loan paid 
        loanAmount = 0
    }
    else {
        loanAmount -= 0.1 * pay                                     // If there is a loan, 10% of pay goes to loan
        balance += 0.9 * pay                                        // 90% of pay goes to balance
    }
    pay = 0

    if (loanAmount > 0) {                                           // If there is a loan   
        loanElement.innerText = getCurrency(loanAmount)                 // Shows loan amount element
    }
    else {                                                          // If there is no active loan
        hide(loanElement, loanTextElement, repayButton)             // Hides loan details and repay button
    }

    balanceElement.innerText = getCurrency(balance)
    payElement.innerText = getCurrency(pay)
}

bankButton.addEventListener("click", bank)

// Loan button action
function getLoan() {
    if (loanAmount === 0) {                                         // If there is no active loan
        while (true) {                                              // checks the validation of the input
            loanAmount = prompt("Enter the amount you want to loan");
            if (loanAmount == null || loanAmount == "" || loanAmount == 0  ) {           // No input
                alert("You did not enter anything. Please enter an amount again.");
            }
            else if (isNaN(loanAmount)) {                           // Not numeric input
                alert("Invalid input. Please enter an amount again.");
            }
            else {
                break
            }
        }

        if (balance * 2 < loanAmount) {                             // Checks if loan amount is at least 2 X balance value
            alert("Your balance is too low to loan " + getCurrency(loanAmount))
            loanAmount = 0
        }
        else {                                                      // If it is, shows loan details and repay button
            show(loanElement, loanTextElement, repayButton)         // Increases balance's amount by loan's amount
            loanElement.innerText = getCurrency(loanAmount)
            balance += loanAmount * 1
            balanceElement.innerText = getCurrency(balance)
        }
    }
    else {
        alert("You cannot get more than one bank loan before repaying the last loan")
    }
}

getLoanButton.addEventListener("click", getLoan)

// Repay button action
function repay() {
    if (pay === 0  ) {           // No input
        alert("Your pay is $0.00. No credits to transfer");
    }
    if (pay < loanAmount) {                                         // If pay is less than the loan amount
        loanAmount -= pay                                           // decreases loan's amount by pay's amount
        pay = 0
        loanElement.innerText = getCurrency(loanAmount)
        payElement.innerText = getCurrency(pay)
    }
    else {                                                          // If pay is grater than the loan amount
        balance += pay - loanAmount                                 // remaining funds after paying the loan are transferred to the balance
        loanAmount = 0
        pay = 0
        hide(loanElement, loanTextElement, repayButton)             // hides loan details and repay button too
        payElement.innerText = getCurrency(pay)
        balanceElement.innerText = getCurrency(balance)
    }
}

repayButton.addEventListener("click", repay)

// Buy button action
function buy() {
    console.log("Balance: " + balance)
    console.log("Price: " + price)

    if (balance >= price) {                                         // If there are enough credits
        balance -= price                                            // balance is decreased by the price
        balanceElement.innerText = getCurrency(balance)
        alert("You are now the owner of the new computer!")
    }
    else {
        alert("You cannot afford the computer")
    }
}

buyButton.addEventListener("click", buy)

// return the currency of a number
function getCurrency(number) {
    return Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(number)
}

// functions to make an element visible or hidden
function hide(...elements) {
    for (e of elements)
        e.style.visibility = 'hidden'
}

function show(...elements) {
    for (e of elements)
        e.style.visibility = 'visible'
}